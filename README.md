# [partnerka.org](https://partnerka.org) source codes

<br/>

### Run partnerka.org on localhost

    # vi /etc/systemd/system/partnerka.org.service

Insert code from partnerka.org.service

    # systemctl enable partnerka.org.service
    # systemctl start partnerka.org.service
    # systemctl status partnerka.org.service

http://localhost:4028
